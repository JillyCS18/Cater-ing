from django.test import TestCase
from django.urls import resolve, reverse
from home.views import homepage

# Create your tests here.
class Homepage_Unit_Test(TestCase):
    def test_homepage_is_exist(self):
        response = self.client.get(reverse('home:homepage'))
        self.assertEqual(response.status_code, 200)

    def test_qna_using_qna_func(self):
        url = resolve('/')
        self.assertEqual(url.func, homepage)
