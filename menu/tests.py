from django.test import TestCase
from django.urls import resolve, reverse
from qna.models import Answer
from qna.views import menu

# Create your tests here.
class Menupage_Unit_Test(TestCase):
    def test_menu_is_exist(self):
        response = self.client.get(reverse('menu:menu'))
        self.assertEqual(response.status_code, 200)

    def test_menu_using_menu_func(self):
        url = resolve('/menu/')
        self.assertEqual(url.func, menu)
    
    def test_model_can_create_new_menu(self):
        new_menu = Menu.objects.create(
            image = 'steak_veggie.jpg',
            name = 'Steak',
            menu_type = 'Main Course',
            price = 'Rp45.000,00',
            desc = 'Lorem ipsum dolor'
        )

        self.assertEqual(Menu.objects.all().count(),1)
    
    def test_menu_GET(self):
        response = self.client.get(reverse('menu:menu'))
        self.assertTemplateUsed(response, 'menu.html')
