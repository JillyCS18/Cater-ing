from django.test import TestCase
from django.urls import resolve, reverse
from qna.models import Answer
from qna.views import qna


# Create your tests here.
class Qna_Unit_Test(TestCase):
    def test_qna_is_exist(self):
        response = self.client.get(reverse('qna:qna'))
        self.assertEqual(response.status_code, 200)

    def test_qna_using_qna_func(self):
        url = resolve('/qna/')
        self.assertEqual(url.func, qna)

    def test_model_can_create_new_answer(self):
        new_answer = Answer.objects.create(key='pesan', answer='bisa hubungi kami di +62-812-9128-0150')
        self.assertEqual(Answer.objects.all().count(),1)

    def test_qna_GET(self):
        response = self.client.get(reverse('qna:qna'))
        self.assertTemplateUsed(response, 'qna.html')

    def test_qna_POST(self):
        Answer.objects.create(
            key='order',
            answer='bisa hubungi kami di +62-812-9128-0150'
            )
        Answer.objects.create(
            key='notfound',
            answer=''
            )
        response = self.client.post(reverse('qna:qna'), data={'keyword':'order'})

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "bisa hubungi kami")
