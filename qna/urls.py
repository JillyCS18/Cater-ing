from django.urls import path
from qna import views

app_name = 'qna'

urlpatterns = [
    path('', views.qna, name='qna'),
]
